import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Scanner;

import static java.lang.Character.getNumericValue;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {

//        Урок 5. StringBuilder, обработка строк.
//                Цель задания:
//        Знакомство с конструктором StringBuilder, совершенствование навыков работы
//        со строками
//        Задание:
//        1.
//        Пользователь вводит стихотворение из четырёх строк. Соберите их в одну
//        переменную, между каждой строкой вставьте символ переноса строки: «
//\
//        n»

        System.out.println("!!!!!!!!!!!!!!!!! 1 !!!!!!!!!!!!!!!!!");
        System.out.println("введите стихотворение");
        StringBuilder stb = new StringBuilder();
        Scanner scaner = new Scanner(System.in);
        for (int i=0;i<4;i++){
         stb.append(scaner.nextLine()+"\n");
        }

        System.out.println(stb.toString());

//        2.
//        Пользователь вводит три слова, соберите из них строку, где слова будут в
//        обратном порядке

        System.out.println("!!!!!!!!!!!!!!!!! 2 !!!!!!!!!!!!!!!!!");
        System.out.println("введите три слова");
        StringBuilder stb2= new StringBuilder();
        Scanner scaner2 = new Scanner(System.in);
        for (int i=0;i<3;i++){
            stb2.insert(0,scaner2.nextLine());
        }

        System.out.println(stb2.toString());


//        3.
//        Пользователь вводит сло
//        во. Добавьте к нему в начало «вы говорите:» ...
//        слово пользователя. И в конец: «.. и что?»
        System.out.println("!!!!!!!!!!!!!!!! 3 !!!!!!!!!!!!!!!!!");
        StringBuilder stb3 = new StringBuilder("Вы говорите:  и что?");
        Scanner scanner3 = new Scanner(System.in);
        System.out.println("Введите слово:");
        stb3.insert(13, scanner3.nextLine());
        System.out.println(stb3.toString());


//        4.
//        Пользователь вводит пять слов, соберите из них целую строку, между
//        каждым словом вставьте «, и»

        System.out.println("!!!!!!!!!!!!!!!! 4 !!!!!!!!!!!!!!!!!");
        StringBuilder stb4 = new StringBuilder();
        Scanner scanner4 = new Scanner(System.in);
        System.out.println("Введите 5 слов:");
        for (int i=0;i<5;i++){
            if (i<4)
            stb4.append(scanner4.nextLine()+", и ");
            else
                stb4.append(scanner4.nextLine());
        }
        System.out.println(stb4.toString());


//        5.
//        Пользователь вводит предложение их двух слов (считайте в одну
//        переменну
//        ю String phrase). Вставьте между этими словами «так сказать»
//        используя StringBuilder.insert. Было: «Учу Java». Станет: «Учу, так сказать,
//                Java»

        System.out.println("!!!!!!!!!!!!!!!! 5 !!!!!!!!!!!!!!!!!");
        Scanner scanner5 = new Scanner(System.in);

        System.out.println("Введите предложение из двух слов:");
        StringBuilder stb5 = new StringBuilder(scanner5.nextLine());

        stb5.insert(stb5.toString().indexOf(' '),", так сказать,");

        System.out.println(stb5.toString());

//        6.
//        Пользователь вводит число, любое. 237, например. Выведите: 237
//        программистов, окончание должно зависеть от числа
//. 1 программист, 2
//        программиста, и так далее

        System.out.println("!!!!!!!!!!!!!!!!!!!!!! 6 !!!!!!!!!!!!!!!!!!!!!");
        System.out.println("введите число");
        StringBuilder stb6 = new StringBuilder();
        int pr6=scanner4.nextInt();
        stb6.append(pr6);
        int n6 = getNumericValue(stb6.charAt(stb6.toString().length()-1));
        if (n6>1&&n6<5) stb6.append(" программиста");
        else if (n6>=5) stb6.append(" программистов");
        else if (n6==1) stb6.append(" программист");
        System.out.println(stb6.toString());


//        7.
//        Пользователь вводит предложение, удалите все пробелы из него

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!! 7 !!!!!!!!!!!!!!!!!!");
        Scanner scanner7 = new Scanner(System.in);
        StringBuilder stb7 = new StringBuilder();
        System.out.println("Введите предложение");
        String str7 = scanner7.nextLine();
        stb7.append(str7);

        int pr7;
        while (true) {
            pr7=stb7.indexOf(" ");
            if (pr7>0)
            stb7.deleteCharAt(pr7);
            else break;
        }
        System.out.println(stb7.toString());
//        8.
//        Вернитесь к программе, которая запрашивает курс валют. Напишите
//        генератор даты для запроса к апи, на основе даты, введенной пользователем.
//                Используйте
//        StringBuilder.

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!8!!!!!!!!!!!!!!!!!!!!!!!");


        Scanner scanner8 = new Scanner(System.in);
        StringBuilder stb8_1 = new StringBuilder();


        System.out.println("Введите год в формате 2024");
        String year8=scanner8.nextLine();
        stb8_1.insert(0,"."+year8);

        System.out.println("Введите месяц,июнь например: 06");
        stb8_1.insert(0,"."+scanner8.nextLine());

        System.out.println("Введите число месяца, например: 08");
        stb8_1.insert(0,scanner8.nextLine());

        String parsString8=downloadWeb("http://www.cbr.ru/currency_base/dynamics/?UniDbQuery.Posted=True&UniDbQuery.so=1&UniDbQuery.mode=1&UniDbQuery.date_req1=&UniDbQuery.date_req2=&UniDbQuery.VAL_NM_RQ=R01235&UniDbQuery.From="+stb8_1.toString()+"&UniDbQuery.To="+stb8_1.toString());
        StringBuilder stb8_2 = new StringBuilder(parsString8);


            String date8="<td>"+stb8_1.toString()+"</td>        <td>1</td>        <td>";
            int Start = stb8_2.toString().lastIndexOf(date8);

            String kurs8=stb8_2.toString().substring((Start+date8.length()),(Integer.parseInt(year8)>1997)?(Start+date8.length()+7):(Start+date8.length()+10));
            System.out.println(stb8_1.toString()+" = (" + kurs8 + ")");



//        9.
//        Скачайте цитату из breaking bad и замените все плохие слова звёздочками


        System.out.println("!!!!!!!!!!!!!!!!!!!!!! 9 !!!!!!!!!!!!!!!!!!!!!!!!!!!");
        String page = downloadWeb("https://favqs.com/api/qotd");
//        System.out.println(page);
        int qStart=page.lastIndexOf("\",\"body\":\"");
        String quote= page.substring(qStart+10,page.length()-3);
        System.out.println("Цитата: "+quote);
        String[] words = quote.split(" ");

       StringBuilder rez8 = new StringBuilder();

        for (String str8 : words){
            if (cenzArr(str8))
                rez8.append(setCenz(str8)+" ");
            else
                rez8.append(str8+" ");
        }
        System.out.println(rez8.toString());

        int aStart=page.lastIndexOf("\"author_permalink\"");
        String authorQ=page.substring(aStart+20,qStart);
        System.out.println("Автор: ("+authorQ+")");





//        10.
//        Напишите крестики
//        -
//                нолики, используя StringBuilder

        //0 1 2
        //3 4 5
        //6 7 8
        char [] oxy = new char[9];
        char vxy = 0;
        System.out.println("!!!!!!!!!!!!!!!!!!!!!! 10 !!!!!!!!!!!!!!!!!!!!!!!!");
        int i10=0;
        int getChar;
        Scanner scanner10 = new Scanner(System.in);
        StringBuilder stb10 = new StringBuilder("   \n"+"   \n"+"   ");

       int index=0;
        do {
           if (i10%2==0) vxy='X';
           else vxy='Y';

           System.out.println("1   2   3");
           System.out.println("4   5   6");
           System.out.println("7   8   9");
           System.out.println("Введите номер ячеки куда запишем:" + String.valueOf(vxy));
           getChar=scanner10.nextInt();
           if (getChar<4)
//               stb10.insert(getChar-1,vxy);
               index=getChar-1;
           else if (getChar>=4&&getChar<=6)
               //с учётом символа переноса строки получается без +1
//               stb10.insert(getChar+1,vxy);
               index=getChar;
           else if (getChar>=7&&getChar<=9)
               //с учётом  второго символа переноса строки получается с +1
//               stb10.insert(getChar+3,vxy);
               index=getChar+1;

           stb10.replace(index,index+1,Character.toString(vxy));
           i10++;
       } while (getResult(stb10)=='0'&&stb10.toString().indexOf(" ")>0);

        //        11.
//        Пусть пользователь вводит поля класса FileInformation, но в одну строку;
//        формат придумайте сами. Считайте ввод пользо
//        вателя и из него создайте
//        экземпляр класса.

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!! 11 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println("Введите строку из двух слов, это будут поля класса:");
        StringBuilder stb11 = new StringBuilder();
        Scanner scanner11 = new Scanner(System.in);
        FileInformation fi11 = new FileInformation(" "," ");
        String[] fff=stb11.append(scanner11.nextLine()).toString().split(" ");

        while (fff.length!=2) {
            System.out.println("вы ввели недостаточно слов.");
            System.out.println("Введите строку из двух слов, это будут поля класса:");
            stb11.delete(0,stb11.toString().length());
            fff=stb11.append(scanner11.nextLine()).toString().split(" ");
        }
        fi11.filename=fff[0];
        fi11.type=fff[1];

        System.out.println("экземпляр класса создан успешно"+fi11);
        System.out.println("имя файла:");
        System.out.println(fi11.filename);
        System.out.println("тип файла:");
        System.out.println(fi11.type);

//        Критерии оценивания:
//        1 балл
//                -
//                создан новый проект в IDE
//        2 балла
//                -
//                написан скелет кода для ввода данных со стороны пользователя
//        3 балла
//                -
//                выполнено более 60% заданий, имеется не более 5 критичных
//                замечаний
//        4 балла
//                -
//                выполнено корректно более 80% технических заданий
//        5 баллов
//                -
//                все технические задания выполнены корректно, в полном объеме
//        Задание считается выполненным при условии, что слушатель получил оценку не
//        менее 3 баллов

    }

    public static String downloadWeb(String urlStr) {
        StringBuilder result = new StringBuilder();
        String line;
        try {
            URL url = new URL(urlStr);
            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
            while ((line = reader.readLine()) != null) result.append(line);
            reader.close();
        } catch (Exception e) {
// ...
            System.out.println(e.getMessage());
        }

        return result.toString();
    }

    static String setCenz(String str){
       StringBuilder ss = new StringBuilder();
        for (int i=0; i<str.length();i++){
           ss.insert(0,'*');
       }
        return ss.toString();
    }

//на вход подаём проверяемое слово/ если true значит слово подлежит цензуре
static boolean cenzArr(String str) {
        //здесь в cear можно добавить "плохие" слова для примера возьмём эти(все с прописной буквы)
        String [] cear = {"bad","is","i","this","fault","never","blame"};
        StringBuilder sb = new StringBuilder();

        for (String cr : cear) {
            boolean equals = cr.equals(str.toLowerCase());
            if (equals) return true;
        }


        return false;
    }
//проверка состояния игры в крестики нолики
static char getResult(StringBuilder stb){
        //если нет победителя возвращаем '0'
        //иначе победитель либо 'X' либо 'Y'
        // мы получаем данные в таком формате
        //("   \n" +"   \n"  +  "   \n");
        //победитель будет если будет одинаковый символ X или Y
        // в следующих комбинациях где число это индекс в строке символ переноса \n занимает 1 индекс
        //0 1 2 (/n)
        //4 5 6 (/n)
        //8 9 10
        //(0/1/2)(4/5/6),(8/9/10),(0/4/8),(1/5/9),(2,6,10),(0,5,10),(8,5,2)

    System.out.println("Текущее состояние игры");
    System.out.println(stb);
        char [] xy = {'X','Y'};
        for (int i=0;i<2;i++)
            if (
                    ((stb.charAt(0) == xy[i])&&(stb.charAt(1) == xy[i])&&(stb.charAt(2) == xy[i]))
                            || ((stb.charAt(4) == xy[i])&&(stb.charAt(5) == xy[i])&&(stb.charAt(6) == xy[i]))
                            || ((stb.charAt(8) == xy[i])&&(stb.charAt(9) == xy[i])&&(stb.charAt(10) == xy[i]))
                            || ((stb.charAt(0) == xy[i])&&(stb.charAt(4) == xy[i])&&(stb.charAt(8) == xy[i]))
                            || ((stb.charAt(1) == xy[i])&&(stb.charAt(5) == xy[i])&&(stb.charAt(9) == xy[i]))
                            || ((stb.charAt(2) == xy[i])&&(stb.charAt(6) == xy[i])&&(stb.charAt(10) == xy[i]))
                            || ((stb.charAt(0) == xy[i])&&(stb.charAt(5) == xy[i])&&(stb.charAt(10) == xy[i]))
                            || ((stb.charAt(8) == xy[i])&&(stb.charAt(5) == xy[i])&&(stb.charAt(2) == xy[i]))
                    ) {
                System.out.println("победа за: "+xy[i]+" !!!!");
                return xy[i];
            }
    System.out.println("Пока ничья!!!");
            return '0';
    }

   static class FileInformation{
        private String filename;
        private String type;
        public FileInformation(String filename, String type){
            this.filename=filename;
            this.type=type;
        }
       // Геттеры для доступа к приватным полям (необязательный шаг)
       public String getName() {
           return this.filename;
       }
       public String getType() {
           return this.type;
       }
    }
}